from django.db import models
from taggit.managers import TaggableManager
from django_extensions.db.fields import AutoSlugField

class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    slug = AutoSlugField('slug', unique=True, populate_from=['title'])
    body = models.TextField()
    tags = TaggableManager()

    def __unicode__(self):
        return self.title
