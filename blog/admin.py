from django.contrib import admin
from blog.models import Post


class PostModel(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'tags', 'body']}),
    ]
    list_display = ('title', 'body',)
    list_filter = ['created']
    search_fields = ['title', 'body']
    date_hierarchy = 'created'

admin.site.register(Post, PostModel)
