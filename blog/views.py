from django.template import Context, loader
from django.http import HttpResponse
from django.shortcuts import render_to_response
from blog.models import Post
from datetime import datetime
import os.path


def tagname(request, tag):
    posts = Post.objects.filter(tags__name=tag).order_by('-created')
    return render_to_response('blog/tag_list.html', {'posts': posts,
                                                     'tag': tag})


def arsip(request):
    posts = Post.objects.order_by('-created').all()
    return render_to_response('blog/arsip.html', {'posts': posts})


def arsip_index(request):
    """Fungsi untuk membuat halaman index"""
    posts = Post.objects.filter().order_by('-created')
    now = datetime.now()

    # create a dict with the year and months:post
    post_dict = {}
    for i in range(posts[0].created.year,
                posts[len(posts) - 1].created.year - 1,
                -1):
        post_dict[i] = {}
        for month in range(1, 13):
            post_dict[i][month] = []
    for post in posts:
        post_dict[post.created.year][post.created.month].append(post)

    # This is necessary for the years to be sorted
    post_sorted_keys = list(reversed(sorted(post_dict.keys())))
    list_posts = []
    for key in post_sorted_keys:
        adict = {key: post_dict[key]}
        list_posts.append(adict)

    t = loader.get_template('blog/post_arsip.html')
    c = Context({
        'now': now,
        'list_posts': list_posts
    })
    return HttpResponse(t.render(c))


def view_static(request):
    static = os.path.abspath(os.path.join(
        os.path.dirname(__file__), '../static')),
    t = loader.get_template('blog/view_static.html')
    c = Context({
        'static': static,
        })
    return HttpResponse(t.render(c))
