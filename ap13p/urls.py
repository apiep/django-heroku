from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView
from django.views.generic.simple import direct_to_template
from django.contrib import admin
from blog.models import Post

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$',
        ListView.as_view(
            queryset=Post.objects.order_by('-created').all()[:5],
            context_object_name='posts',
        )
    ),
    url(r'^blog/(?P<slug>[a-zA-Z0-9-]+)/$',
        DetailView.as_view(
            model=Post
        ),
        name='post_detail'
    ),
    url(r'blog/tag/(?P<tag>\w+)/$',
        'blog.views.tagname'
    ),
    url(r'^arsip/$',
        'blog.views.arsip',
    ),
    # url(r'^arsip/new/$',
    #     'blog.views.arsip_index',
    # ),
    url(r'^tentang/$',
        direct_to_template,
        {'template': 'blog/tentang.html'},
    ),
    url(r'^kontak/$',
        direct_to_template,
        {'template': 'blog/kontak.html'},
    ),
    url(r'^view_static/$',
        'blog.views.view_static'
    )
)

urlpatterns += patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

from django.conf import settings
urlpatterns += patterns('',
    url(r'^static/(?P<s>[.*])$',
        'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}
    ),
)
